package com.telemetry.testgitlab.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	
	@GetMapping("/test")
	public ResponseEntity<String> helloWorldController()
	{
		return new ResponseEntity<String>("Hi ..This is success message from TestTelemetry api in PCF", HttpStatus.OK);
	}

}
